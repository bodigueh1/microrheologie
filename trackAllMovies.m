function trackAllMovies(varargin)
% compute particle tracking on multiple files located in one folder, using same particle detection and tracking parameters for all movies 
% 
% USAGE : 
% ---------
%   trackAllMovies(...)
%   
%   
% INPUT ARGS (all arg are optional)
% ------------------
%        'folder2analyze' (default empty, will open a dialog box if empty)
%               path to the folder containing the movies     
%         'filenamefilter' (default '*.nd2') 
%                filter for the filenames to analyse
%         'foldertosave' (default empty)
%               path to the folder where the results will be saved. If
%               empty, a folder will be created 
%         'foldertoparams'
%               path to the folder where the parameters of the movie will be saved. If
%               empty, a folder will be created 
%         'fileextparams' (default '_params')
%               filename extension for files parameters  
%         'fileextresults' (default '_results')
%               filename extension for files results  
%         'analyzeonlynewmovies' (default true)
%               if true, only movies without corresponding results will be
%               analyzed. if false, all movies will be reanalyzed
%         'minTrackLength' (default 3)
%               option for tracking 
%         'maxDisp' (default 10)
%               option for tracking
%         'bpass'   (default [0.5 5])
%               bandpass parameters of the images   
%         'Imin'    (default 100)
%               threshold for particle detection   
%         'excludedsize' (default 4)
%               excluded size for particle detection   
%          
% EXAMPLES
% ---------
%   trackAllMovies('minTrackLength',10)
%   trackAllMovies('minTrackLength',10,'Imin',500)
%   trackAllMovies('folder2analyze','C:\data','Imin',500,'minTrackLength', 5)


%default values (could be modified using arguments in the function call):
fileNameFilter='*.nd2';
folderToAnalyze='';
folderToSave='';
folderToParams='';
fileExtParams='_params';
fileExtResults='_results';
analyzeOnlyNewMovies=true;  
minTrackLength=3;
maxDisp=10;
bpass=[0.5 5];
Imin=100;
excludedSize=4;


for (i=1:2:length(varargin))
    switch  lower(varargin{i}) 
        case 'folder2analyze'
            folderToAnalyze=varargin{i+1};
        case 'filenamefilter'
            fileNameFilter=varargin{i+1};
        case 'foldertosave'
            folderToSave=varargin{i+1};
        case 'foldertoparams'
            folderToSave=varargin{i+1};
        case 'fileextparams'
            fileExtParams=varargin{i+1};
        case 'fileextresults'
            fileExtResults=varargin{i+1};
        case 'analyzeonlynewmovies'
            analyzeOnlyNewMovies=varargin{i+1};
        case 'mintracklength'
            minTrackLength=varargin{i+1};
        case 'maxdisp'
            maxDisp=varargin{i+1};
        case 'bpass'
            bpass=varargin{i+1};
        case 'imin'
            Imin=varargin{i+1};
        case 'excludedsize'
            excludedsize=varargin{i+1};

    end
end


if length(folderToAnalyze)==0
    folderToAnalyze=uigetdir();
end

if length(folderToParams)==0
    [p,f]=fileparts(folderToAnalyze);
    folderToParams=fullfile(p,[f fileExtParams]);
end

if length(folderToSave)==0
    [p,f]=fileparts(folderToAnalyze);
    folderToSave=fullfile(p,[f fileExtResults]);
end


d=dir(fullfile(folderToAnalyze,fileNameFilter));

if ~isfolder(folderToSave)
    mkdir(folderToSave)
end
if ~isfolder(folderToParams)
    mkdir(folderToParams)
end


for i=1:length(d)
   %look for existing result file
   [~,filename,~]=fileparts(d(i).name); 
   if ~isfile(fullfile(folderToSave,[filename fileExtResults '.mat'])) || ~analyzeOnlyNewMovies   % this will only analyze new files
        
       disp(['Analyzing file : ' d(i).name])
       vid=hb_avi2(fullfile(folderToAnalyze,d(i).name));
       vid.postOperations.bpass=1;
       vid.postOperations.pkfnd=1;
       vid.postOperationsInfos.bpass=bpass;
       vid.postOperationsInfos.pkfnd.Imin=Imin;
       vid.postOperationsInfos.pkfnd.size=excludedSize;

       warning('off')
       save(fullfile(folderToParams,[filename fileExtParams '.mat']),'vid');
       warning('on')
       partPositions=hb_locatePart(vid);
       
       str=sprintf('%g particles detected / frame',length(partPositions)/length(vid.time));
       disp(str)
       
       optstr=struct('mem',0,'good',round(minTrackLength),'quiet',true,'dim',2);
       [~,I] = sort(partPositions(:,3));
        try
           partPositions=track(partPositions(I,1:3),round(maxDisp),optstr);
           disp(sprintf('Trackind done. Mean track length : %g',getMeanTrackLength(partPositions)));
            save(fullfile(folderToSave,[filename fileExtResults '.mat']),'partPositions');
            disp('--------------- Results saved successfully --------------')
        catch ME 
             disp(ME.message)
             disp('--------------- File was skipped --------------- ')
     	end
       
       
   end
    
end