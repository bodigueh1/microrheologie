function cnt2=hb_locatePart(vid)

cnt2=[];
h=hb_waitbar();
for i=1:length(vid.time)
 vid=vid.read(i);
 cnt=cntrd(vid.currentIm,vid.postOperationsInfos.pkfnd.results,ceil((vid.postOperationsInfos.bpass(2)+2)/2)*2-1);
 cnt2=[cnt2; [cnt(:,1:2) i*ones(size(cnt,1),1)]];
 if mod(i,5)==0
     hb_waitbar(h,i/length(vid.time));
 end
end
close(h)

end