function b=getMeanTrackLength(tr)

if (isempty(tr))
    b=0;
    return
end

%a=max(tr((tr(:,3)==1),4));
%disp ([num2str(a) ' part visibles sur la premi�re image. ']);

b=length(tr(:,4))/max(tr(:,4));

%disp ([ 'Longueur moyenne d''un track : ' num2str(b) '.']);

end