function res=hb_diffusion_partRaffournier(A,opts) %hb_diffusion_part gives out a structure "res" containing : tau, deltaR�, D, alpha, beta, x, y for every particles and every time step

%Manual use of the function, with loaded vid in Workspace
%opts=struct('tauStep',1, 'tauLength',floor(length(vid.time)/4), 'fit',1, 'dispRaw',true, 'dispStats',true, 'dispMap',true, 'frameRate',vid.aviInfo.FramesPerSecond,'styleMap',2, 'statsDisplay', true, 'filterHisto', false,'filterValue', 1,'scale', vid.aviInfo.PixelSizeX*1e-6)

%Search in resultCorPerPart.part.tau the index matching the tau that you desire as a display point for deltar2 histogram
ini_index = 10; 
valeur_tau_ini = 0.1;

%A is a matrix of N lines and 4 columns :
%Column1 : PositionX
%Column2 : PositionY
%Column3 : Time of detection t
%Column4 : Particles ID

%If no arguments are provided by the user, default parameters apply
if (nargin<=1)
    opts.affichage=true;    %
    opts.tauLength=44;       %
    opts.tauStep=1;         %Time step
    opts.frameRate=60;      %Video frame rate
    opts.tailleFilm=120;    %
    opts.scale=1;           %
    opts.fit=1;             %Fit = 1 : powerlaw fit; fit=2 linear fit;
    opts.dispRaw=true;      %
    opts.dispStats=true;    %
    opts.dispMap=false;     %
    opts.styleMap=2;        %Style = 1 : gradient map; style=2 points map
    opts.statsDisplay=true; %Display geometrical mean value and standard deviation on graphs
    opts.filterHisto=false; %If maxBarCount is the maximum count of a value in the log(Deltar2) histogram, allow the filtering of the weaker bars
    opts.filterValue=1;     %If filterHisto is activated, filter all bars that are inferior to filterValue% of maxBarCount
end
correctMeanDisp=true;       %Apply a correction to the individual MSD based on the common MSD in order to take into account any drifting
                            %This will solve the drifting if it is based of an homogeneous velocity field, but not if different drifting apply in the study zone


%**************************************************************************
%INITIALIZATION OF PARTICLES AND TIME SCALES
%**************************************************************************
%Number of particles // A(:,4) corresponds to the particle ID column in A ; So its maximum is the total number of studied particles
Np=max(A(:,4));

%collecte des correlations et rescaling
h=waitbar(0,'Correlations in progress...');

%Vector going from tauStep (initial time) to tauLenght (final time) with a time step of tauStep
tau=opts.tauStep:opts.tauStep:opts.tauLength;
%Vector going from tauStep to tauLenght, with 100 values distributed in a log10 fashion
tauinterp=logspace(log10(opts.tauStep),log10(opts.tauLength),100);

res.part=struct('tau',[],'Deltar2',[],'Deltar2_ini',[],'logDeltar2_ini',[],'D',[],'alpha',[],'beta',[],'x',[],'y',[]);

if  opts.dispRaw
    figure(52)
    clf
end

%**************************************************************************
%WHOLE PARTICLE FIELD MEAN DISPLACEMENT COMPUTATION
%**************************************************************************
%Compute mean displacement between each time interval (at which at least one displacement was detected)
if correctMeanDisp
    %Nt corresponds to the lattest time at which a particle was detected
	Nt=max(A(:,3)); 
    %Nt-1 lines because the lattest detected point cannot be paired with a next point to form a displacement
    uimoy=zeros(Nt-1,2)*NaN; 
    
    for t=1:Nt-1
        %For every time interval (t to t+1), extract all the data measured at this time interval 
        %Time interval, so Nt-1 intervals between Nt points
        Pi1=A(A(:,3)==t,:);
        Pi2=A(A(:,3)==t+1,:);
        %Empty ui for every new analyzed time interval t
        ui=[];
        
        %Scroll through every particle detected in Pi1 at time t
        for k=1:length(Pi1(:,4))
            %Find in Pi1 the first particle of similar ID, detected at time t+1 // record its ID as k0
            %If no match is found (no continuity between t and t+1 for this given particle, then k0 = 0)
            k0=find(Pi2(:,4)==Pi1(k,4),1);
            %As explained, if k0=0 then the particle found at time t was not found again at time t+1 // Hence no displacement can be computed
            if (k0>0)
                %Add another displacement in the list of all displacements recorded at the time interval t to t+1
                %Similar particle, so index k for Pi1 and k0 for Pi2
                ui=[ui ; (Pi2(k0,1:2)-Pi1(k,1:2))];
            end
        end
        
        %If several displacements were detected at the chosen time interval, compute the mean value
        %If not, the single value is similar to a mean value anyway
        if (size(ui,1)>1)
                %Compute displacements mean value
                uim=mean(ui);
                %Add this mean value to the list, with the corresponding time t at which this mean displacement will happen
                %Line ID : t
                %2 columns, meanDisplacementX and meanDisplacementY
                uimoy(t,:)=uim;
        end
        
        
        %Add [0 0] as the initial position
        %Adding the displacements dX and dY at every point, allows to track the mean position of the particle over the field of view
        %Starting from [0 0], a mean displacement [3 4] between t1 and t2 leads to mean position [3 4]
        %Then, re-starting from [3 4], a mean displacement [5 2] between t2 and t3 leads to mean position [8 6]
        meanpos=[[0 0]; cumsum(uimoy,1)];
    end
    %At the end, we obtain the mean displacement of all detected particle for each time interval. 
    %This value will be used to correct the individual particle displacement
end

%**************************************************************************
%DRIFTING CORRECTION
%**************************************************************************
figure(52)
%Loop on all the particles in order to compute Delta r^2
for i=1:Np  
    %Traj is made of the 3 first columns (X, Y, t) of the i-chosen particle tracked locations matrix
    traj=A(A(:,4)==i,1:3);
    %Empty Deltar2 matrix of the same size as the number of time steps tau
    Deltar2=zeros(size(tau))*NaN;
    
    NN=sum(tau<length(traj)/5); % do not unerstand
    %Loop on every tau values
    for j=1:NN 
        u2=[];
        for t=min(traj(:,3)):max(traj(:,3))-tau(j)
           
            u=traj(traj(:,3)==t+tau(j),1:2)-traj(traj(:,3)==t,1:2);
            if (correctMeanDisp)
                u=u-(meanpos(t+tau(j),1:2)-meanpos(t,1:2));
            end
            if ~isempty(u)
                u2=[u2 u(1)^2+u(2)^2];
            end
        end
        Deltar2(j)=mean(u2)*opts.scale^2;  
    end
    Deltar2_ini(i)=Deltar2(ini_index);
    %ln is log() and log is log10() in MATLAB
    logDeltar2_ini(i)=log10(Deltar2_ini(i));
    
%**************************************************************************
%INDIVIDUAL MSD GRAPH CONSTRUCTION AND ALPHA/D COMPUTATION
%**************************************************************************  

    %**************************************************************************
    %MSD GRAPH
    %**************************************************************************
    %In any case, the MSD must be computed to estimate other values such as alpha or D
    %However, the MSD graph is only displayed if the user ask for it
    if  opts.dispRaw
        plot(tau/opts.frameRate,Deltar2,'-','displayname',sprintf('Np=%g, length %g',i,length(traj)));
        %Add every new particle-line on the same graph
        hold on
        %For the first particle, define the axis as log scale (no need to redo it for every other lines)
        if i==1
              set(gca, 'XScale','log')
              set(gca, 'YScale','log')
        end
    end
   
    %**************************************************************************
    %ALPHA OR DIFFUSION COEFFICIENT D COMPUTATION
    %**************************************************************************
    if opts.fit==1 %Power-law, so log(MSD) ~ alpha.log(tau), the value of alpha will be estimated and D will be ignored
        %Extract more Deltar2 values associated to the virtual tauinterp points, using the real Deltar2 values for the real tau values
        Deltar2interp = interp1(tau,Deltar2,tauinterp);
        
        %~isnan(Deltar2interp) : creates a matrix within which cells are equal to True if the corresponding cell in Deltar2interp is not equal to NaN
        %tauinterp(~isnan(Deltar2interp)) : select the values of tauinterp for which the corresponding cell of Deltar2interp is not equal to NaN
        %length(tauinterp(~isnan(Deltar2interp)))>2 : the interpolation can only happen if there is more than 2 points available
        if length(tauinterp(~isnan(Deltar2interp)))>2
            %polyfit(x, y, n) fits a polynomial of degree n to the data points (x, y)
            %In that case, it is a linear fit (n=1) because we are working in log-log scale, so a power-law correspond to a linear graph
            %x : log(tauinterp(~isnan(Deltar2interp))/opts.frameRate) : log of all tauinterp values corresponding to Deltar2interp values that are not equal to NaN
            %y : log(Deltar2interp(~isnan(Deltar2interp)) : log of all Deltar2interp that are not equal to NaN
            resfit=polyfit(log(tauinterp(~isnan(Deltar2interp))/opts.frameRate),log(Deltar2interp(~isnan(Deltar2interp))),1);
        %If not enough points could be interpolated, ignore the alpha vakue for this particle
        else
            resfit=[NaN NaN];
        end
        %The result resfit contains two values for a linear fit :
        %resfit(1) : the slope (alpha)
        alpha(i)=resfit(1);
       
        res.part(i)=struct('tau',tau/opts.frameRate,'Deltar2',Deltar2,'Deltar2_ini',Deltar2_ini(i),'logDeltar2_ini', logDeltar2_ini(i),'D',NaN,'alpha',alpha(i),'beta',exp(resfit(2)),'x',mean(traj(:,1)),'y',mean(traj(:,2)));
        %END OF THE POWER-LAW LOG(MSD) = ALPHA.LOG(TAU) CASE
        
    elseif opts.fit==2 %Linear law, so MSD = 4.D.tau in 2 dimensiosn (x,y)
        %Extract more Deltar2 values associated to the virtual tauinterp points, using the real Deltar2 values for the real tau values
        Deltar2interp = interp1(tau,Deltar2,tauinterp);
        
        %~isnan(Deltar2interp) : creates a matrix within which cells are equal to True if the corresponding cell in Deltar2interp is not equal to NaN
        %tauinterp(~isnan(Deltar2interp)) : select the values of tauinterp for which the corresponding cell of Deltar2interp is not equal to NaN
        %length(tauinterp(~isnan(Deltar2interp)))>2 : the interpolation can only happen if there is more than 2 points available
        if length(tauinterp(~isnan(Deltar2interp)))>2
            %polyfit(x, y, n) fits a polynomial of degree n to the data points (x, y)
            %In that case, it is a linear fit (n=2) because we are working in a linear scale
            %x : tauinterp(~isnan(Deltar2interp))/opts.frameRate : all tauinterp values corresponding to Deltar2interp values that are not equal to NaN
            %y : Deltar2interp(~isnan(Deltar2interp) : all Deltar2interp that are not equal to NaN
            resfit=polyfit(tauinterp(~isnan(Deltar2interp))/opts.frameRate,Deltar2interp(~isnan(Deltar2interp)),1);
        %If not enough points could be interpolated, ignore the alpha vakue for this particle
        else
            resfit=[NaN NaN];
        end  
        %The result resfit contains two values for a linear fit :
        %resfit(1) : the slope D/4 (because MSD = 4.D.tau)
        D(i)=resfit(1)/4;  
        
        res.part(i)=struct('tau',tau/opts.frameRate,'Deltar2',Deltar2,'Deltar2_ini',Deltar2_ini(i),'logDeltar2_ini', logDeltar2_ini(i),'D',D(i),'alpha',NaN,'beta',NaN,'x',mean(traj(:,1)),'y',mean(traj(:,2)));
        %END OF THE LINEAR MSD = 4.D.TAU CASE
    end
    
    %The waitbar runs until all particles have been processed
    waitbar(sqrt(i/Np),h);
end


%**************************************************************************
%PARTICLE deltar� GRAPH
%**************************************************************************
if (opts.dispRaw)
    set(gca, 'XScale','log')
    set(gca, 'YScale','log')
    xlabel('\tau (s)')
    ylabel('\Delta r^2 (m^2)')
    xlim([0.01 0.4]) 
    ylim([10^(-17) 10^(-13)]) 
end
close(h);

%**************************************************************************
%ALPHA & deltar� HISTOGRAMS
%**************************************************************************
saveHistoRaffournier(res, opts.dispStats, opts.fit, valeur_tau_ini, opts.filterHisto, opts.filterValue);


%**************************************************************************
%PARTICLES MAP
%**************************************************************************
if  opts.dispMap
    figure(54)
    %If no map xInterval and yInterval has been defined yet, the map limits are defined as the maximum value of X and Y detected in a trajectory.
    %It is useless to plot a wider map, as no trajectory would be visible in the added parts.
    if ~isfield(opts,'sizeX')
        opts.sizeX=max(ceil(A(:,1)));
        opts.sizeY=max(ceil(A(:,2)));
    end
    %Create two matrix X and Y, representing a grid with axis going from 1 to sizeX and 1 to sizeY
    [X,Y] = ndgrid(1:opts.sizeX,1:opts.sizeY);
    
    if opts.styleMap == 1 %Gradient map
        %If the fit is a powerlaw (fit=1) then the power value alpha is used to plot the map
        if opts.fit==1
            F = scatteredInterpolant([res.part.x]',[res.part.y]',[res.part.alpha]');
        %If not, then the fit is linear and the diffusion coefficient is used to plot the map
        else
            F = scatteredInterpolant([res.part.x]',[res.part.y]',[res.part.D]');
        end
        im=F(X,Y);
        imagesc(im)
        caxis([0 1]);
        res.map=im;
    end
    
    if opts.styleMap == 2 %Colored points map
        %If the fit is a powerlaw (fit=1) then the power value alpha is used to plot the map
        if opts.fit==1
            F = [res.part.alpha];
        %If not, then the fit is linear and the diffusion coefficient D is used to plot the map
        else
            F = [res.part.D];
        end
        scatter([res.part.x], [res.part.y], 50, F, 'filled', 'MarkerEdgeColor', 'k');
        
        %For points with alpha < 1, apply the basic color scheme
        colorbar; 
        c.Label.String = 'Alpha'; 
        caxis([0 1]);
        
        axis([0 opts.sizeX 0 opts.sizeY]);
        xlabel('X-axis');  % X-axis label
        ylabel('Y-axis');  % Y-axis label
        c = colorbar;  % Get colorbar handle
        c.Label.String = 'Alpha';  % Label for the colorbar
        res.map = F;
    end
end



end
