function saveHistoRaffournier(result, stats, fit, tau_ini, filter, filterVal)

%USER INPUT VALUES ********************************************************
forceAxis = true;       %deltar2 histogram value will be between axMin and axMax
axMin = (-17);          %User defined xmin for log(deltar2) histogram
axMax = (-13);          %User defined xmax for log(deltar2) histogram
%*************************************************************************

%Default values
if isempty(tau_ini)
    tau_ini = 0.1; 
end
if isempty(fit)
    fit = 1; 
end
if isempty(stats)
    stats = true;
end
if isempty(filter)
    filter = false;
end
filteredHisto = false;  %If the user force the deltar2 axis and hide some values, show it in the label

%Retrieve data from resultCorPerPart
for i=1:size(result.part,2)
    valAlpha(i) = result.part(i).alpha;
    valD(i) = result.part(i).D;
    valLogDeltar2_ini(i) = result.part(i).logDeltar2_ini;
end

%**************************************************************************
%ALPHA HISTOGRAM
%**************************************************************************
figure(53) %53 matches the figure index used in trackAllMovies_Raffournier
if fit==1 %Power-law log(MSD) ~ alpha.log(tau)
    edges = linspace(0, 2, 21); %By definition, alpha values are stuck between 0 and 2
    h=histogram(valAlpha, 'BinEdges', edges); 
    xlabel('alpha');
    ylabel('Count');  
else %Linear law MSD = 4.D.tau
    h=histogram(valD, 20);
    xlabel('Diffusion coefficient D');  
    ylabel('Count');  
end

if  stats
    %Add the statistical informations on the graph
    hold on;
    %Compute the geometrical mean and the standard deviation
    binCenters = h.BinEdges(1:end-1) + diff(h.BinEdges)/2;
    binCounts = h.Values;
    gm = exp(sum(binCounts .* log(binCenters)) / sum(binCounts));
%     variance = sum(binCounts .* (binCenters - gm).^2) / sum(binCounts);
%     std_dev = sqrt(variance);
    %**************************************************************************
    %GEOMETRICAL MEAN
    %**************************************************************************    
    %Plot the geometrical mean as a vertical line
    plot([gm gm], [0 max(binCounts)], 'k', 'LineWidth', 1); 
    %Show the geometrical mean value on top of the graph
    textPositionX = gm;
    textPositionY = max(binCounts)*1.05;
    text(textPositionX, textPositionY, ['geomean_alpha = ' num2str(gm, '%.2f')], ...
    'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', ...
    'Color', 'k', 'FontSize', 12, 'Interpreter', 'none');
    %Widen a bit the vertical dimensions of the graph to leave space for the text
    ylim([0, max(binCounts)*1.1]);
    
    %**************************************************************************
    %INIDIVIDUAL STANDARD DEVIATION
    %************************************************************************** 
%     %Plot the standard deviation error bars
%     ypos = max(binCounts) * 0.9;
%     plot([gm - std_dev, gm + std_dev], [ypos ypos], 'k', 'LineWidth', 1); 
%     %Add the error bar "wings" 
%     smallBarLength = 1;
%     plot([gm - std_dev, gm - std_dev], [ypos - smallBarLength, ypos + smallBarLength], 'k', 'LineWidth', 1); %Left part
%     plot([gm + std_dev, gm + std_dev], [ypos - smallBarLength, ypos + smallBarLength], 'k', 'LineWidth', 1); %Right part
end

%Primary and secondary ticks intervals
majorTicks = 0:1:2; 
minorTicks = 0:0.1:2; 
ax = gca; 
ax.XTick = majorTicks; 
ax.XMinorTick = 'on'; 
ax.XRuler.MinorTickValues = minorTicks; 
%Thicker axis - to see both the bars and the ticks
ax.LineWidth = 1.5; 

%res.histogram=h;


%**************************************************************************
%DELTAR� HISTOGRAM
%**************************************************************************
figure(55); %55 matches the figure index used in trackAllMovies_Raffournier
%Define the xlim and xmax of the histogram, accordingly to logDeltar2 non-NaN values
%Retrieve the minimum and maximum value from the data
%All the log(Deltar2) values are negative and will be rounded to the lesser 0.1 value (-2.88 -> -2.9 for example)
minVal = min(valLogDeltar2_ini(~isnan(valLogDeltar2_ini)));
minVal = floor(minVal*10)/10;
maxVal = max(valLogDeltar2_ini(~isnan(valLogDeltar2_ini)));
maxVal = floor(maxVal*10)/10;
%Three possibility :
%A : the reference values interval is INSIDE the data extrema interval, and the user doesn't force the axis
%   => The axis interval is the data extrema interval [minVal ; maxVal]
%   Also possible to have [minVal ; axMax] or [axMin ; maxVal] if the two intervals are superimposed
%B : the reference values interval is INSIDE the data extrema interval, but the user force the axis to his reference
%   => The axis interval is the user interval [axMin ; axMax]
%C : the reference values interval is OUTSIDE the data extrema interval
%   => The axis interval is the user interval [axMin ; axMax]
if (minVal < axMin && forceAxis) || (maxVal > axMax && forceAxis)
    %In that condition, the forcing of the axis limits by the user will hide some data
    %In order to avoid to loose some data by error, it will change the axis names in order to show this data loss
    filteredHisto = true;
end
if minVal > axMin || forceAxis
    minVal = axMin;
end
if maxVal < axMax || forceAxis
    maxVal = axMax;
end

%One bar every 0.1 // (+1) because there is (N-1) bars for N edges
%If interval of 3, then 31 edges, so 30 bars (30*0.1 = 3)
nbEdges = (maxVal-minVal)*10+1;
edges2 = linspace(minVal, maxVal, nbEdges); 
h2 = histogram(valLogDeltar2_ini,'BinEdges', edges2);

if  stats
    %Add the statistical informations on the graph
    hold on;
    %Compute the geometrical mean and the standard deviation
    binEdges2 = h2.BinEdges;
    binCenters2 = binEdges2(1:end-1) + diff(binEdges2)/2;
    binCounts2 = h2.Values;
    gm2 = -exp(sum(binCounts2 .* real(log(binCenters2))) / sum(binCounts2));
%     variance2 = sum(binCounts2 .* (binCenters2 - gm2).^2) / sum(binCounts2);
%     std_dev2 = sqrt(variance2);
    %**************************************************************************
    %GEOMETRICAL MEAN
    %**************************************************************************
    %Plot the geometrical mean as a vertical line
    plot([gm2 gm2], [0 max(binCounts2)], 'k', 'LineWidth', 1); 
    %Show the geometrical mean value on top of the graph
    textPositionX = gm2;  
    textPositionY = max(binCounts2)*1.05; 
    deltar2_mean = real(gm2);
    text(textPositionX, textPositionY, ['geomean_deltar� = ' num2str(deltar2_mean, '%.2f')], ...
    'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', ...
    'Color', 'k', 'FontSize', 12, 'Interpreter', 'none');
    %Widen a bit the vertical dimensions of the graph to leave space for the text
    ylim([0, max(binCounts2)*1.1]);
    
    %**************************************************************************
    %INIDIVIDUAL STANDARD DEVIATION
    %**************************************************************************
%     %Plot the standard deviation error bars
%     ypos2 = max(binCounts2)*0.9; % Position verticale pour la barre d'�cart-type
%     plot([gm2 - std_dev2, gm2 + std_dev2], [ypos2 ypos2], 'k', 'LineWidth', 1); % Barre horizontale pour l'�cart-type
%     %Add the error bar "wings" 
%     smallBarLength2 = 1; 
%     plot([gm2 - std_dev2, gm2 - std_dev2], [ypos2 - smallBarLength2, ypos2 + smallBarLength2], 'k', 'LineWidth', 1); %Left part
%     plot([gm2 + std_dev2, gm2 + std_dev2], [ypos2 - smallBarLength2, ypos2 + smallBarLength2], 'k', 'LineWidth', 1); %Right part
    
end

%Primary and secondary ticks intervals
majorTicks = minVal:1:maxVal; 
minorTicks = minVal:0.1:maxVal; 
ax = gca; 
ax.XTick = majorTicks; 
ax.XMinorTick = 'on'; 
ax.XRuler.MinorTickValues = minorTicks; 
%Thicker axis - to see both the bars and the ticks
ax.LineWidth = 1.5; 

%Label the axis
if filteredHisto 
    xlabel(['log10(deltar2(' num2str(tau_ini) 's))_filtered' ], 'Interpreter', 'none');
else
    xlabel(['log10(deltar2(' num2str(tau_ini) 's))']);
end
%The count of value is not affected by the x-axis limits
ylabel('Count');  

%res.histogram2=h2;


%**************************************************************************
%MULTIPLE ALPHA AND deltar2 HISTOGRAM
%**************************************************************************
figure(57); %55 matches the figure index used in trackAllMovies_Raffournier for multiple alpha histograms
if fit==1 %Power-law log(MSD) ~ alpha.log(tau)
    h3=histogram(valAlpha, 'BinEdges', edges); 
    xlabel('alpha');
    ylabel('Count');  
else %Linear law MSD = 4.D.tau
    h3=histogram(valD, 20);
    xlabel('Diffusion coefficient D');  
    ylabel('Count');  
end
figure(58); %58 matches the figure index used in trackAllMovies_Raffournier for multiple deltar2 histograms
h4 = histogram(valLogDeltar2_ini,'BinEdges', edges2);
%Label the axis
xlabel(['log10(deltar2(' num2str(tau_ini) 's))']);
ylabel('Count');  

%**************************************************************************
%FILTER LOW OCCURENCE VALUES
%**************************************************************************
if filter
    figure(56);
    % Cr�er l'histogramme
    Nmax = max(binCounts2);
    disp(Nmax);
    % Trouver les bins o� le d�compte est sup�rieur � 5% de Nmax
    threshold = filterVal*Nmax/100;
    valid_bins = binCounts2  >= threshold;
    valid_edges = h2.BinEdges([1, find(valid_bins) + 1]);  % Ajouter 1 pour ajuster l'indexation des bacs
    % R�cup�rer les valeurs des deux extr�mit�s de l'axe des X
    x_min = valid_edges(1);  % Premi�re valeur de l'axe X (bords du premier bin)
    x_max = valid_edges(end);  % Derni�re valeur de l'axe X (bords du dernier bin)
    % Cr�er 20 bins entre x_min et x_max
    edges_20_bins = linspace(x_min, x_max, 21);  % 21 bords pour 20 bins
    % Calculer les comptages des bins dans l'intervalle [x_min, x_max] avec 20 bins
    [counts_20_bins, ~] = histcounts(valLogDeltar2_ini, edges_20_bins);
    % Tracer l'histogramme filtr�
    bar(edges_20_bins(1:end-1), counts_20_bins, 'histc');
    xlabel(['log10(deltar2(' num2str(tau_ini) 's))_filtered'], 'Interpreter', 'none');
    ylabel(['Count_filtered(' num2str(filterVal) '%)'], 'Interpreter', 'none');  
end


end