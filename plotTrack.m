function plotTrack(tr,fig1)

if (nargin==1)
    fig1=gcf;
    clf
end
    
    
hold on


for i=1:max(tr(:,4))

    m=tr(:,4)==i;
   x=tr(m,1);
   y=tr(m,2);
   
   if (length(x)>3)
       plot(x,y,'color',[rand(1,1) rand(1,1) rand(1,1)],'lineWidth',1);
       hold on
   end
   
    
end
hold off


% for (i=1:max(tr(pim,4)))
%   
%     aa=find(tr(:,4)==i);
%  %   plot(tr(aa,2)*240/512,tr(aa,1)*240/512,'color',[0.99 0.99 0.99],'lineWidth',2);
%       plot(tr(aa,1),tr(aa,2),'color',[0 0 0],'lineWidth',2);
%     hold on;
%     
% end
% hold off
% end