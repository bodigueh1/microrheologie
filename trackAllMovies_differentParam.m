function trackAllMovies_differentParam(varargin)
% compute particle tracking on multiple files located in one folder, using
% different particle detection parameters for all files. 
% the function trackAllMovies_differentParam_init() must have been called
% before in order to create all parameter files. File for which there is no
% param file will not be analyzed.
% 
% USAGE : 
% ---------
%   trackAllMovies_differentParam(...)
%   
%   
% INPUT ARGS (all arg are optional)
% ------------------
%        'folder2analyze' (default empty, will open a dialog box if empty)
%               path to the folder containing the movies     
%         'filenamefilter' (default '*.nd2') 
%                filter for the filenames to analyse
%         'foldertosave' (default empty)
%               path to the folder where the results will be saved. If
%               empty, a folder will be created 
%         'foldertoparams'
%               path to the folder containing the parameters of the movie. 
%         'fileextparams' (default '_params')
%               filename extension for files parameters  
%         'fileextresults' (default '_results')
%               filename extension for files results  
%         'analyzeonlynewmovies' (default true)
%               if true, only movies without corresponding results will be
%               analyzed. if false, all movies will be reanalyzed
%         'minTrackLength' (default 3)
%               option for tracking 
%         'maxDisp' (default 10)
%               option for tracking
% EXAMPLES
% ---------
%   trackAllMovies_differentParam('minTrackLength',10)
%   trackAllMovies_differentParam('minTrackLength',10,'maxdisp',20)
%   trackAllMovies_differentParam('folder2analyze','C:\data','minTrackLength',10,'maxdisp',20)


%default values (could be modified using arguments in the function call:
fileNameFilter='*.nd2';
folderToAnalyze='';
folderToSave='';
folderToParams='';
fileExtParams='_params';
fileExtResults='_results';
analyzeOnlyNewMovies=true;  
minTrackLength=3;
maxDisp=10;


for (i=1:2:length(varargin))
    switch  lower(varargin{i}) 
        case 'folder2analyze'
            folderToAnalyze=varargin{i+1};
        case 'filenamefilter'
            fileNameFilter=varargin{i+1};
        case 'foldertosave'
            folderToSave=varargin{i+1};
        case 'foldertoparams'
            folderToSave=varargin{i+1};
        case 'fileextparams'
            fileExtParams=varargin{i+1};
        case 'fileextresults'
            fileExtResults=varargin{i+1};
        case 'analyzeonlynewmovies'
            analyzeOnlyNewMovies=varargin{i+1};
        case 'mintracklength'
            minTrackLength=varargin{i+1};
        case 'maxdisp'
            maxDisp=varargin{i+1};
    end
end

if isempty(folderToAnalyze)
    folderToAnalyze=uigetdir();
end

if isempty(folderToParams)
    [p,f]=fileparts(folderToAnalyze);
    folderToParams=fullfile(p,[f fileExtParams]);
end

if isempty(folderToSave)
    [p,f]=fileparts(folderToAnalyze);
    folderToSave=fullfile(p,[f fileExtResults]);
end

d=dir(fullfile(folderToAnalyze,fileNameFilter));

if ~isfolder(folderToSave)
    mkdir(folderToSave)
end

for i=1:length(d)
   %look for existing result file
   [~,filename,~]=fileparts(d(i).name); 
   if ~isfile(fullfile(folderToSave,[filename fileExtResults '.mat'])) || ~analyzeOnlyNewMovies   % this will only analyze new files
       if isfile(fullfile(folderToParams,[filename fileExtParams '.mat']))
            vid=importdata(fullfile(folderToParams,[filename fileExtParams '.mat']));
            if ~isa(vid,'hb_avi2') 
                break
            end
            disp(['Analyzing file : ' d(i).name])
            partPositions=hb_locatePart(vid);
       
            str=sprintf('%g particles detected / frame',length(partPositions)/length(vid.time));
            disp(str)
       
           optstr=struct('mem',0,'good',round(minTrackLength),'quiet',true,'dim',2);
           [~,I] = sort(partPositions(:,3));
            try
               partPositions=track(partPositions(I,1:3),round(maxDisp),optstr);
               disp(sprintf('Trackind done. Mean track length : %g',getMeanTrackLength(partPositions)));
                save(fullfile(folderToSave,[filename fileExtResults '.mat']),'partPositions');
                disp('--------------- Results saved successfully --------------')
            catch ME 
                 disp(ME.message)
                 disp('--------------- File was skipped --------------- ')
            end
       else
            disp(['Missing parameter file for movie ' d(i).name])
            disp('--------------- File was skipped --------------- ')
       end
       
       
   end
    
end