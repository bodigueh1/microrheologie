function trackAllMovies_differentParam_init(varargin)
% this function is made to define particle detection parameters on multiple
% file. A dialog box will be open for each file located in a given folder
% allowing to set and save bpass and particle detection parameters
% 
% USAGE : 
% ---------
%   trackAllMovies_differentParam_init(...)
%   
%   
% INPUT ARGS (all arg are optional)
% ------------------
%        'folder2analyze' (default empty, will open a dialog box if empty)
%               path to the folder containing the movies     
%         'filenamefilter' (default '*.nd2') 
%                filter for the filenames to analyse
%         'foldertosave' (default empty)
%               path to the folder where the parameters will be saved. If
%               empty, a folder will be created 
%         'fileextparams' (default '_params')
%               filename extension for files parameters  
%         'analyzeonlynewmovies' (default true)
%               if true, only movies without corresponding results will be
%               analyzed. if false, all movies will be reanalyzed
%          
% EXAMPLES
% ---------
%   trackAllMovies_differentParam_init('filenamefilter','test*.nd2')
%   trackAllMovies_differentParam_init('folder2analyze','C:\data','filenamefilter','test*.nd2')





%default values (could be modified using arguments in the function call:
fileNameFilter='*.nd2';
folderToAnalyze='';
folderToSave='';
fileExtParams='_params';
analyzeOnlyNewMovies=true;  


for (i=1:2:length(varargin))
    switch  lower(varargin{i}) 
        case 'foldertoanalyze'
            folderToAnalyze=varargin{i+1};
        case 'filenamefilter'
            fileNameFilter=varargin{i+1};
        case 'foldertosave'
            folderToSave=varargin{i+1};
        case 'fileextparams'
            fileExtParams=varargin{i+1};
        case 'analyzeonlynewmovies'
            analyzeOnlyNewMovies=varargin{i+1};
    end
end

if length(folderToAnalyze)==0
    folderToAnalyze=uigetdir();
end

if length(folderToSave)==0
    [p,f]=fileparts(folderToAnalyze);
    folderToSave=fullfile(p,[f fileExtParams]);
end




bpass=[0.5 5];
Imin=0; % if 0, will be set automatically for the first file
excludedSize=4;





d=dir(fullfile(folderToAnalyze,fileNameFilter));

if ~isfolder(folderToSave)
    mkdir(folderToSave)
end

for i=1:length(d)
   %look for existing result file
   [~,filename,~]=fileparts(d(i).name); 
   if ~isfile(fullfile(folderToSave,[filename fileExtParams '.mat'])) || ~analyzeOnlyNewMovies   % this will only analyze new files
       vid=hb_avi2(fullfile(folderToAnalyze,d(i).name));
       vid.postOperations.bpass=1;
       vid.postOperationsInfos.bpass=bpass;
       vid=vid.read(1);
 
       vid.postOperations.pkfnd=1;
       if Imin==0
           Imin=round(max(vid.currentIm(:))/4);
       end
       vid.postOperationsInfos.pkfnd=struct('Imin',Imin,'size',excludedSize,'results',[]);
       h=hb_video214(vid);
       hdhb_video=guidata(h);
       set(h,'CloseRequestFcn',{@handleStateChange,fullfile(folderToSave,[filename fileExtParams '.mat']),filename})

       uiwait(h)
   end
    
end


function handleStateChange(varargin)

hdhb_video=guidata(varargin{1});
vid=hdhb_video.video;
warning('off')
save(varargin{3},'vid');
warning('on')
delete(varargin{1})
disp(['Parameter File created for file : ' varargin{4}])
Imin=vid.postOperationsInfos.pkfnd.Imin;
excludedSize=vid.postOperationsInfos.pkfnd.size;
bpass=vid.postOperationsInfos.bpass;
end

end

